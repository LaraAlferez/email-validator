package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import sheridan.EmailValidator;

public class EmailValidatorTest {

	/******** Test has correct format ******/
	@Test
	public void testHasValidFormatRegular() {
		boolean result = EmailValidator.isValidEmail("Test@hotmail.com");
		assertTrue("Invalid format email", result);
	}

	@Test
	public void testHasValidFormatlException() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("Invalid format email", result);
	}

	@Test
	public void testHasValidFormatBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("test@123.ca");
		assertTrue("Invalid format email", result);
	}

	@Test
	public void testHasValidFormatBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("testhotmail.com");
		assertFalse("Invalid format email", result);
	}

	/******** Test has one symbol only ******/
	@Test
	public void testHasOneSymbolRegular() {
		boolean result = EmailValidator.isValidEmail("test@hotmail.com");
		assertTrue("Invalid number of symbols", result);
	}

	@Test
	public void testHasOneSymbolException() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("Invalid number of symbols", result);
	}

	@Test
	public void testHasOneSymbolBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("hello@example.com");
		assertTrue("Invalid number of symbols", result);
	}

	@Test
	public void testHasOneSymbolBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("test@@hotmail.com");
		assertFalse("Invalid number of symbols", result);
	}

	/******** Account name requirements ******/

	@Test
	public void testIsValidAccountNameRegular() {
		boolean result = EmailValidator.isValidEmail("abc123@example.com");
		assertTrue("Invalid account name", result);
	}

	@Test
	public void testIsValidAccountNameException() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("Invalid account name", result);
	}

	@Test
	public void testIsValidAccountNameBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("abdc@example.com");
		assertTrue("Invalid account name", result);
	}

	@Test
	public void testIsValidAccountNameBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("1abc@example.com");
		assertFalse("Invalid account name", result);
	}

	/******** Domain name requirements ******/
	@Test
	public void testIsValidDomainNameRegular() {
		boolean result = EmailValidator.isValidEmail("hello@gmail.com");
		assertTrue("Invalid email", result);
	}

	@Test
	public void testIsValidDomainNameException() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("Invalid email", result);
	}

	@Test
	public void testIsValidDomainNameBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("abdc@exa.com");
		assertTrue("Invalid email", result);
	}

	@Test
	public void testIsValidDomainNameBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("abdc@ex.com");
		assertFalse("Invalid email", result);
	}

	/******** Extension name requirements ******/

	@Test
	public void testIsValidExtensionNameRegular() {
		boolean result = EmailValidator.isValidEmail("hello@example.com");
		assertTrue("Invalid extension name", result);
	}

	@Test
	public void testIsValidExtensionNameException() {
		boolean result = EmailValidator.isValidEmail(null);
		assertFalse("Invalid extension name", result);
	}

	@Test
	public void testIsValidExtensionNameBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("hello@example.ca");
		assertTrue("Invalid extension name", result);
	}

	@Test
	public void testIsValidExtensionNameBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("hello@exa.ca112");
		assertFalse("Invalid extension name", result);
	}
}
