/*
 *@author Lara Alferez, 991540084
 *This class validates emails and it will be developed using TDD,SCM & CI
 *
 *Instructions
 *1. Email must be in this format: <account>@<domain>.<extension> 
 *2. Email should have one and only one @ symbol. 
 *3. Account name should have at least 3 alpha�characters in lowercase (must not start with a number).
 *4. Domain name should have at least 3 alpha�characters in lowercase or numbers. 
 *5. Extension name should have at least 2 alpha�characters (no numbers).
 *
 *	 Email validation should be implemented through a class method in the Java class, named isValidEmail and takes a String argument.
 *	 Note that not all 5 requirements above will need all 4 testing scenarios, yet a minimum of 15 unit tests are required.
 *
 */

package sheridan;

public class EmailValidator {

	public static boolean isValidEmail(String email) {
		String regex = "^([^0-9][a-z0-9]{3,})*@([a-z0-9-]{3,}+\\.)+[a-zA-Z]{2,}$";
		return email != null && email.matches(regex);
	}

}
